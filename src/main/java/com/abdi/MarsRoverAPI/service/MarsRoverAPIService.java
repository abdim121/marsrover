package com.abdi.MarsRoverAPI.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.abdi.MarsRoverAPI.response.MarsRoverAPIResponse;

@Service
public class MarsRoverAPIService {
	private static final String API_KEY = "fHtlU1acO7Kmlwdyc3JKv9dAPqqA3pFYMMF91JAG";
	public MarsRoverAPIResponse getRoverData(String roverType) {
		RestTemplate rt = new RestTemplate();
		
		ResponseEntity<MarsRoverAPIResponse> response = rt.getForEntity("https://api.nasa.gov/mars-photos/api/v1/rovers/"+roverType+"/photos?sol=2&api_key="+API_KEY, MarsRoverAPIResponse.class);
		return response.getBody();
	}
}
