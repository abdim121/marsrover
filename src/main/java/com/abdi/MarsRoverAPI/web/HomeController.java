package com.abdi.MarsRoverAPI.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abdi.MarsRoverAPI.response.MarsRoverAPIResponse;
import com.abdi.MarsRoverAPI.service.MarsRoverAPIService;

@Controller
public class HomeController {
	
	@Autowired
	private MarsRoverAPIService roverService;
	
	@GetMapping("/") //Access local-host root.
	public String getHomeView(ModelMap model, @RequestParam(required=false) String marsRoverApiData) { //ModelMap extends LinkedHashMap. Required=false means that this request is not always needed. 
		// if request param is empty, then set a default value.
		if(StringUtils.isEmpty(marsRoverApiData)) {
			marsRoverApiData = "opportunity";
		}
		MarsRoverAPIResponse roverData = roverService.getRoverData(marsRoverApiData);
		model.put("roverData", roverData);
		return "index"; //this is the view to be rendered. 
	}
	
	
}
